/*
 * $HeadURL: https://svn.apache.org/repos/asf/httpcomponents/oac.hc3x/trunk/src/test/org/apache/commons/httpclient/TestHttps.java $
 * $Revision: 608014 $
 * $Date: 2008-01-02 16:48:53 +1100 (Wed, 02 Jan 2008) $
 * ====================================================================
 *
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 * ====================================================================
 *
 * This software consists of voluntary contributions made by many
 * individuals on behalf of the Apache Software Foundation.  For more
 * information on the Apache Software Foundation, please see
 * <http://www.apache.org/>.
 *
 * [Additional notices, if required by prior licensing conditions]
 *
 */

package org.apache.commons.httpclient;

import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

/**
 * Simple tests for HTTPS support in HttpClient.
 *
 * To run this test you'll need:
 *  + a JSSE implementation installed (see README.txt)
 *  + the java.protocol.handler.pkgs system property set
 *    for your provider.  e.g.:
 *     -Djava.protocol.handler.pkgs=com.sun.net.ssl.internal.www.protocol
 *    (see build.xml)
 *
 * @author Rodney Waldhoff
 * @author Ortwin Glück
 * @version $Id: TestHttps.java 608014 2008-01-02 05:48:53Z rolandw $
 */
public class TestHttps {
    private static String SNI_EXTENSION_PROPERTY_KEY = "jsse.enableSNIExtension";

    @Before
    public void setUp() {
        System.setProperty(SNI_EXTENSION_PROPERTY_KEY, "true");
    }

    @After
    public void tearDown() {
        System.clearProperty(SNI_EXTENSION_PROPERTY_KEY);
    }

    public static void configureProxy(HttpClient client) {
        final String proxyHost = System.getProperty("httpclient.test.proxyHost");
        final String proxyPort = System.getProperty("httpclient.test.proxyPort");
        final String proxyUser = System.getProperty("httpclient.test.proxyUser");
        final String proxyPass = System.getProperty("httpclient.test.proxyPass");
        if (proxyHost != null) {
            if (proxyUser != null) {
                HttpState state = client.getState();
                state.setProxyCredentials(AuthScope.ANY, new UsernamePasswordCredentials(
                    proxyUser, proxyPass));
            }
            client.getHostConfiguration().setProxy(proxyHost, Integer.parseInt(proxyPort));
        }
    }

    public void assertHttpsGetSuccess(HttpClient client, String url) throws IOException {
        GetMethod method = new GetMethod(url);
        method.getParams().setParameter(
            HttpMethodParams.RETRY_HANDLER, new NoRetryHttpMethodRetryHandler());
        client.executeMethod(method);
        String data = method.getResponseBodyAsString();
        // This enumeration musn't be empty
        assertTrue("No data returned.", (data.length() > 0));
    }

    @Test
    public void testHttpsGet() throws IOException {
        HttpClient client = new HttpClient();
        configureProxy(client);
        assertHttpsGetSuccess(client, "https://www.verisign.com:443/");
    }

    @Test
    public void testHttpsGetNoPort() throws IOException {
        HttpClient client = new HttpClient();
        configureProxy(client);
        assertHttpsGetSuccess(client, "https://www.verisign.com/");
    }

    /**
     * Warning this test depends on an external system for the purposes of testing
     * SNI support.
     */
    @Ignore("Need to find different SNI test uri.")
    @Test
    public void testHttpsGetSNIWithConnectionTimeout() throws IOException {
        if (!isJava7OrAbove()) {
            return;
        }
        HttpClient client = new HttpClient();
        configureProxy(client);
        HttpConnectionManagerParams params = client.getHttpConnectionManager().getParams();
        params.setConnectionTimeout(5000);
        assertHttpsGetSuccess(client, "https://sni.velox.ch");
        assertHttpsGetSuccess(client, "https://alice.sni.velox.ch");
    }

    private static boolean isJava7OrAbove() {
        final String version = System.getProperty("java.version");
        double javaVersion = Double.parseDouble(version.substring(0, version.lastIndexOf(".")));
        return javaVersion >= Double.parseDouble("1.7");
    }

    /**
     * A HttpMethodRetryHandler which never retries a connection.
     */
    private class NoRetryHttpMethodRetryHandler implements HttpMethodRetryHandler {

        @Override
        public boolean retryMethod(HttpMethod method, IOException exception, int executionCount) {
            return false;
        }
    }

}
